var Vue = require('vue')

var VueMaterial = require('vue-material')
import 'material-design-icons/iconfont/material-icons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
Vue.use(VueMaterial)
require('vue-material/dist/vue-material.css')

import MyPlugin from './lib.js';
Vue.use(MyPlugin)

new Vue({
    el: "#page",
    render(h) {
        return h(require('./MD.vue'));
    }
})