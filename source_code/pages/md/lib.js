let MyPlugin = {

};

MyPlugin.install = function (Vue, options) {
    // 1. 添加全局方法或属性
    Vue.myGlobalMethod = function (val) {
        // 逻辑...
        alert(val || 213123123);
    }
    // 2. 添加全局资源
    Vue.directive('my-directivexxx', {
        bind(el, binding, vnode, oldVnode) {
            console.log(el);
            console.log(binding);
            console.log(vnode);
            console.log(oldVnode);
            // 逻辑...
        }
    })
    // 3. 注入组件
    Vue.mixin({
        created: function () {
            // 逻辑...
        },
        components: {
            'mc-log': {
                template: "<div>log-->> <slot></slot></div>"
            }
        }
    })
    // 4. 添加实例方法
    Vue.prototype.$myMethod = function (options) {
        // 逻辑...
    }
}

export default MyPlugin;