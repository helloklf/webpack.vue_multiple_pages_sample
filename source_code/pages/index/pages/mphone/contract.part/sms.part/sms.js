let vm = {
    props: ['contact'],
    methods: {
        loadList(pageindex, pagesize) {
            pageindex = pageindex == null ? 0 : pageindex;
            request("local.getsmslist", { pageindex, pagesize }, (r) => {
                r.datas.forEach(item => item._isCurrent = item._selected = false);
                (this.messageList = r.datas);
                if (this.messageList.length == 0)
                    return;
                if (pageindex < 1)
                    this.loadDetails(r.datas[0].recipientIds);
                else
                    this.messageList.join(r.datas);
                if (r.datas.length == pagesize)
                    this.loadList(pageindex + 1, pagesize);

                this.allSelected = false;
            });
        },
        loadDetails(recipientIds, pageindex, pagesize) {
            let messageInfo = this.messageList.where(item => item.recipientIds == recipientIds)[0];
            if (messageInfo != null) {
                
                messageInfo.hasNewMsg = false;
                messageInfo.newmsgCount = 0;

                this.currentContact = messageInfo.recipienName || messageInfo.recipientNumber;
                this.messageList.forEach(item => item._isCurrent = item.recipientIds == recipientIds);
                request("local.getmessages", { recipientIds, pageindex, pagesize }, (r) => {
                    this.messageDetails = r.datas;
                });
            }
        },
        refresh() {
            this.loadList();
        },
        messageListChecked() {
            this.allSelected = this.messageList.length == (this.selectedCount = this.messageList.where(item => item._selected).length);
        },
        messageClick(e) {
            this.loadDetails(e.currentTarget.getAttribute('data-key'));
        },
        deleteSelected() {
            let ids = this.messageList.where(item => item._selected).select(item => item.recipientIds);
            request("smslist.delete", { ids: ids.join('|') }, (r) => {
                alert(删除成功);
            });
        },
        toggleSelectAll(e) {
            let status = e.currentTarget.checked;
            this.selectedCount = status ? this.messageList.length : 0;
            this.messageList.forEach(item => item._selected = status);
        },
        createNew() {
        },
        deleteMsg(e) {
            let key = e.currentTarget.getAttribute('data-key');
            request("sms.delete", { id: key }, (r) => {
                alert(删除成功);
            });
        },
        timeTxt(date) {
            let dateStr;
            let now = new Date();
            if (date.getFullYear() == now.getFullYear()) {
                //判断日期是否为当前年份
                if (date.getDate() == now.getDate()) {
                    //判断日期是否为当前日期，如果是，则显示时分
                    dateStr = date.format('HH:mm');
                } /*else if (weekStartDate <= d && d <= weekEndDate) {
                    //如果是在本周内，则显示星期
                    dateStr = date.format('EEE');
                } */
                else {
                    //如果不在本周内，显示月份+日期
                    dateStr = date.format('MM月dd日');
                }
            } else {
                //如果日期不是当前年份，则显示对应的年月日
                dateStr = date.format('yyyy年MM月dd日');
            }
            return dateStr;
        }
    },
    mounted() {
        this.refresh();
    },
    data() {
        return {
            messageList: [],
            messageDetails: [],
            currentContact: '',
            allSelected: false,
            selectedCount: 0
        };
    }
};

export default vm;