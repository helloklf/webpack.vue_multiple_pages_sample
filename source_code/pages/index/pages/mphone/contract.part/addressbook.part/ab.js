
export default {
    props: ['sms'],
    methods: {
        loadList(pageindex, pagesize) {
            pageindex = pageindex == null ? 0 : pageindex;
            request("contacts.getlist", { pageindex, pagesize }, (r) => {
                r.datas.forEach(item => item._isCurrent = item._selected = false);
                (this.contactsList = r.datas);
                if (this.contactsList.length == 0)
                    return;
                if (pageindex < 1)
                    this.loadDetails(r.datas[0].rawContactId);
                else
                    this.contactsList.join(r.datas);
                if (r.datas.length == pagesize)
                    this.loadList(pageindex + 1, pagesize);

                this.allSelected = false;
            });
        },
        loadDetails(rawContactId) {
            let item = this.contactsList.where(item => item.rawContactId == rawContactId)[0];
            if (item != null) {
                this.contactsList.forEach(item => item._isCurrent = item.rawContactId == rawContactId);
                this.contactDetails = item;
            }
        },
        refresh() {
            this.loadList();
        },
        listItemClick(e) {
            this.loadDetails(e.currentTarget.getAttribute('data-key'));
        },
        listItemChecked() {
            this.allSelected = this.contactsList.length == (this.selectedCount = this.contactsList.where(item => item._selected).length);
        },
        deleteSelected() {
            let ids = this.contactsList.where(item => item._selected).select(item => item.rawContactId);
            request("contacts.delete", { ids: ids.join('|') }, (r) => {
                alert(删除成功);
            });
        },
        toggleSelectAll(e) {
            let status = e.currentTarget.checked;
            this.selectedCount = status ? this.contactsList.length : 0;
            this.contactsList.forEach(item => item._selected = status);
        },
        parseNameType(type) {
            switch (type) {
                case 2: {
                    return "手机";
                }
                case 24: {
                    return "邮箱";
                }
                default: {
                    return "其它";
                }
            }
        },
        createNew() {

        },
        editCurrent() {

        }
    },
    mounted() {
        this.refresh();
    },
    data() {
        return {
            contactsList: [],
            allSelected: false,
            contactDetails: {

            },
            selectedCount: 0
        };
    },
    computed: {

    }
};