import Vue from 'vue'

Vue.config.debug = true;//开启错误提示

var VueMaterial = require('vue-material')
import 'material-design-icons/iconfont/material-icons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
Vue.use(VueMaterial)
require('vue-material/dist/vue-material.css')

import PageContent from "./Page.vue";

new Vue({
  el: '#page',
  data: {
    currentRoute: window.location.pathname
  },
  render(h){
    return h(PageContent);
  }
});