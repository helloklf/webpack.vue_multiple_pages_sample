import contract from './service.contract.js';

export default {
    /* 获取所有联系人消息列表
    Url：http://127.0.0.1:30000/smslist?pagesize=20&start=0
    Response：
    [{
        date: 1492837747125,
        isRead: 1,
        messageCount: 11,
        recipientIds: "16",
        recipients: [
                        0: {
                            number: "95555",
                            rawContactId: 0,
                            type: 0
                        },
                        length: 1
                    ],
        snippet: "您账户5139于04月22日13:08在【支付宝-理财】发生直付通/快捷支付扣款，人民币1000.00[招商银行]",
        snippet_cs: 0,
        snippetType: 1,
        threadId: 16,
        unReadCount: 0
    }]
    */
    "local.getsmslist": new contract.ServiceInfo({
        action: "smslist",
        params({ pageindex, pagesize }) {
            pageindex = pageindex == null ? 0 : pageindex;
            pagesize = pagesize == null ? 100 : pagesize;
            return { pagesize, 'start': pageindex * (pageindex - 1) }
        },
        'reusult': function (res) {
            let arr = [];
            for (let i = 0; i < res.length; i++) {
                let item = res[i];
                arr.push({
                    date: new Date(parseInt(item.date)),
                    hasNewMsg: !(item.isRead),
                    newmsgCount: item.unReadCount,
                    messageCount: item.messageCount,
                    recipientIds: item.recipientIds,
                    recipienName: item.recipients[0].name,
                    recipientNumber: item.recipients[0].number,
                    snippet: item.snippet
                });
            }

            return arr;
        }
    }),
    /* 获取指定联系人的所有消息
    Url：http://127.0.0.1:30000/smslist/${recipientIds}?pagesize=20&start=0 例如：http://127.0.0.1:30000/smslist/16?pagesize=20&start=0
    Response：
    {
        smsinfos:[
            {
                smsinfo:{
                    address: "10010",
                    body: "温馨提示：截止2017年03月20日20时05分，您当月省内流量已用尽，敬请留意。更多流量详情请登录http://cx.17wo.cn 或发送CXLL到10010查询。为让您享受更多优惠，可发送LLRB5（10）到10010订购5元包1GB（10元包3GB）省内流量快餐包，立即生效且当天内有效。推荐您安装【手机营业厅】客户端获取更多流量及优惠信息，下载及查看最新优惠请点击 http://u.10010.cn/yffqtzw 。本次查询结果存在延时，以上信息仅供参考。广东联通",
                    date: 1490011897259,
                    date_sent: 0,
                    error_code: 0,
                    id: 61,
                    locked: 0,
                    personId: 0,
                    protocal: 0,
                    read: 1,
                    seen: 1,
                    status: -1,
                    threadAddress: "10010",
                    threadId: 29,
                    type: 1 //0：全部 1：收件箱，2：已发送，3：草稿，4：发件中，5：发送失败，6：等待中
                }
            }
        ]
    }
    */
    "local.getmessages": new contract.ServiceInfo({
        action: "smslist/${recipientIds}",
        params({ recipientIds, pageindex = 0, pagesize = 100 }) {
            pageindex = pageindex == null ? 0 : pageindex;
            pagesize = pagesize == null ? 100 : pagesize;
            return { pagesize, start: pageindex * (pageindex - 1), recipientIds }
        },
        reusult(res) {
            console.log(res);
            let arr = [];
            let msgs = res.smsinfos;
            for (let i = 0; i < msgs.length; i++) {
                let item = msgs[i].smsinfo;
                arr.push({
                    id: item.id,
                    date: new Date(parseInt(item.date)),
                    body: item.body,
                    id: item.id,
                    isSend: item.type != 1
                });
            }

            return arr;
        }
    }),
    "sms/permission": new contract.ServiceInfo({
        action: "sms.permission",
        params() {
            return { "permission": false, "versionSdk": 23, "defaultSmsApp": "com.android.mms" };
        }, reusult() {

        }
    }),
    /* 删除短信
    Url：http://127.0.0.1:30000/sms/${id} 如 http://127.0.0.1:30000/sms/39
     */
    "sms.delete": new contract.ServiceInfo({
        action: "sms/${id}",
        method: "delete",
        params({ id }) {
            return { id };
        }
    }),
    /* 删除短信-指定多个联系人，
    Url：http://127.0.0.1:30000/smslist/${ids} 如 http://127.0.0.1:30000/smslist/18%7C22 (%7C是转义后的 '|')
     */
    "smslist.delete": new contract.ServiceInfo({
        action: "sms/${ids}",
        method: "delete",
        type: "application/x-www-form-urlencoded",
        params({ ids }) {
            //多个id使用 '|'符分隔，如 12|13|15
            return encodeURIComponent(ids);
        }
    }),


    /*
    获取联系人列表
    Url：http://127.0.0.1:30000/contacts?pagesize=50&start=0
    Response
    {
        "contacts": [
            {
                "contact": {
                    "Id": 0,
                    "accountType": 0,
                    "addresslist": [],
                    "displayName": "陈湘华（同学）",
                    "firstname": "",
                    "lastname": "陈湘华（同学）",
                    "mails": [],
                    "memberShip": "未分组",
                    "middlename": "",
                    "organizations": [],
                    "phoneNumber": [
                        {
                            "data": "176 7461 7667",
                            "name": 2 //2：手机号 24：邮箱
                        }
                    ],
                    "rawContactId": 105,
                    "website": []
                },
                "link": {
                    "href": "/contact/105",
                    "rel": "self"
                }
            }
        ],
        "total": 172
    }
     */
    "contacts.getlist": new contract.ServiceInfo({
        action: "contacts",
        params({ pageindex, pagesize }) {
            pageindex = pageindex == null ? 0 : pageindex;
            pagesize = pagesize == null ? 100 : pagesize;
            return { pagesize, 'start': pageindex * (pageindex - 1) }
        },
        reusult(res) {
            let contacts = res.contacts;
            let arr = [];
            for (let i = 0; i < contacts.length; i++) {
                let item = contacts[i].contact;
                let displayPNumber = (item.phoneNumber && item.phoneNumber.length > 0) ? item.phoneNumber[0].data : "";
                arr.push({
                    rawContactId: item.rawContactId,
                    displayName: item.displayName,
                    displayPNumber,
                    mails: item.mails,
                    memberShip: item.memberShip || '未分组',
                    phoneNumbers: item.phoneNumber,
                    image: item.headPhotoPath ? ('http://127.0.0.1:30000/file' + item.headPhotoPath) : null
                });
            }
            return arr;
        }
    }),
    "contacts.delete": new contract.ServiceInfo({
        action: "http/deleteContacts/${ids}",
        method: "delete",
        params({ ids }) {
            //多个id使用 '|'符分隔，如 12|13|15
            return { ids: encodeURIComponent(ids) };
        }
    })
};