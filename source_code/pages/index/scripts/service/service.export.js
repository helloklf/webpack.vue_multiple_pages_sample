import contract from './service.contract.js';
import ajaxrequest from './service.ajaxrequest.js';
import localapi from './service.localapi.js';

/**
 * @namespace
 */
let service = {
    contract,
    ajaxrequest,
    localapi
};

export default service;