import contract from './service.contract.js';

/**
 * 尝试将一个字符串转换为json
 * @param {String} 符合json格式的字符串 
 * @param {*} 默认值，在转换失败时作为返回值，否则将默认返回jsonString
 * @returns {JSON|String|*} 
 */
function tryParseJson(jsonString, defaultValue) {
    if (jsonString == null || !typeof (jsonString) == 'string')
        return defaultValue || jsonString;

    //是否符合 { ... } 或 [ ... ] 的格式
    if (jsonString != null && /(^[\s]{0,}[{].*[}][\s]{0,}$)|(^[\s]{0,}[\[].*[\]][\s]{0,}$)/.test(jsonString)) {
        try {
            return JSON.parse(jsonString);//eval(`( ${ jsonString } )`);//JSON.parse(jsonString);
        }
        catch (ex) {
            console.log("尝试转换为json失败：" + jsonString);
        }
    }

    return defaultValue || jsonString;
}


/**
 * 格式化url
 * @param {String} url 具有${ prop }风格占位符的 url，如：http://baidu.com/${ action }
 * @param {Object} params 要填充到url占位符的数据，如：{ action:'s/baidu' }
 * @returns {String} 格式化后的url，如 http://baidu.com/s/baidu
 */
function urlFormate(url, params) {
    params = params || {};
    if (!(params instanceof Array)) {
        //验证url中是否有类似 ${ prop }风格占位符
        if (/\$\{[\s]{0,}[_\$A-Za-z]{1,}[_\$A-Za-z\d]{0,}[\s]{0,}[}]/.test(url)) {
            for (let key in params) {
                url = url.replace(new RegExp("\\$\\{[\\s]{0,}" + key + "[\\s]{0,}[}]", 'gi'), params[key]);
            }
        }
    }
    return url;
    //example：urlFormate("http://baidu.com/${ action }",{ action:'s/wd' }) -> "http://baidu.com/s/wd";
}

/**
 * 参数组合到url中，通常实在get请求中，需要将参数添加到url
 * @param {String} url 请求地址，如：http://baidu.con/s 或 http://baidu.con/s#abc 或 http://baidu.con/s?wd=asdf1234#abc 等正确格式url均可
 * @param {Object} params 要整合的参数，如 { act:123123 }
 * @returns {String} 格式化后的url，如 http://baidu.con/s?wd=asdf1234&act=123123#abc
 */
function paramToUrl(url, params) {
    if (url == null || params == null || params.keys().length == 0 || !(params instanceof Object))
        return url;

    //容错处理
    let urlLeft = url;
    let urlRight = '';
    if (url.includes('#')) {
        //作用：http://baidu.com/#abc -> urlLeft = 'http://baidu.com/' , urlRight='#abc'
        let li = url.lastIndexOf('#');
        urlLeft = url.substring(0, li);
        urlRight = url.substring(li, url.length);
    }
    if (!urlLeft.includes('?')) {
        //作用 urlLeft="http://baidu.com/" -> urlLeft="http://baidu.com/?"
        urlLeft = urlLeft + '?';
    }
    else if (!urlLeft.endsWith('&') && !urlLeft.endsWith('?')) {
        //作用 urlLeft="http://baidu.com/?abc=" -> urlLeft="http://baidu.com/?abc=&"
        urlLeft = urlLeft + '&';
    }

    let paramString = "";
    //组合参数
    for (let key in params) {
        let value = encodeURIComponent(params[key] == null ? "" : params[key]);
        paramString += (`${key}=${value}&`);
    }
    //去掉paramString最后一个&
    if (paramString.endsWith('&'))
        paramString.substring(0, (paramString.length - 1));

    let formatedUrl = urlLeft + paramString + urlRight;
    if (formatedUrl.length > 4096)
        throw "参数组合后的url长度超出4096个字符，这是无效的！";

    return formatedUrl;
}

/**
 * 
 * @param {service.contract.ServiceInfo} api 接口信息，包含请求url，请求方式（get、post）等
 * @param {Function} callback function( result:service.contract.ServiceResult ){ ... } 
 * @param {*} datas 要提交的数据内容，default = ""
 */
function ajaxRequest(api, callback, datas = "") {
    let url = urlFormate(api.url, datas);
    let method = (api.method == null ? "GET" : api.method).toUpperCase();
    let async = true;
    let user = null;
    let password = null;
    let type = api.type;
    let form = (api.params && api.params instanceof Function) ? api.params(datas) : datas;
    if (method == "GET")
        url = paramToUrl(url, form);

    //console.log("请求开始：" + url + "\n" + JSON.stringify(form));

    let request = new XMLHttpRequest();
    request.onreadystatechange = function (e) {
        if (request.readyState == 4) {
            //console.log("请求结束：\n" + request.response);
            if (request.status == 200) {
                if (callback && callback instanceof Function) {
                    let tryToJsonResult = tryParseJson(request.response, request.response);

                    let reusult = new contract.ServiceResult({
                        success: true,
                        message: "",
                        datas: ((api.reusult && api.reusult instanceof Function) ? api.reusult(tryToJsonResult) : tryToJsonResult)
                    });
                    try {
                        callback(reusult);
                    }
                    catch (ex) {
                        throw `回调失败 ${ex.message}`;
                    }
                }
            }
            else if (request.status == 0) {
                throw `无法连接，${url}无法通过${method}方式提交内容${datas}`;
            }
            else if (request.status == 403) {
                throw `没有权限，${url}无法通过${method}方式提交内容${datas},`;
            }
            else if (request.status == 404) {
                throw `请求404，${url}无法通过${method}方式提交内容${datas},`;
            }
            else if (request.status == 500) {
                throw `服务器错误，${url}通过${method}方式提交内容${datas}，发生500错误！`;
            }
            else {
                throw `未知错误，${url}通过${method}方式提交内容${datas}发生错误，状态${request.status}！`;
            }
        }
    };


    if (type != null)
        request.setRequestHeader("Content-Type", type);

    request.open(method, url, async, user, password);

    request.send(form);
};


export default ajaxRequest;