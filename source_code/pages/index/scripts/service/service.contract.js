let baseUrl = "http://127.0.0.1:30000/";

/**
 * 一组对于接口信息和请求方式规范化的内容
 * @namespace
 */
let contract = {
    /** 一个标准化的ServiceInfo结构
     * @class
     * @param {string} action 请求路径（相对于baseUrl的路径，如：/users/getlist）.
     * @param {string} url 请求地址（使用完整的地址，如 https://baidu.com/s/wd）.
     * @param {string} method 请求方式.
     * @param {string} type 请求内容的类型（Headers中的Content-Type），不提供则使用默认.
     * @param {Function} params 请求参数处理方法，如果你要在提交前对数据进行一些处理，可以提供一个自定的方法，也可用于规范输入参数.（只在生成body前调用）
     * @param {Function} reusult 请求结果处理方法，如果你需要对请求结果进行一些处理，可以提供一个自定义方法（请求异常不会调用）.
     */
    ServiceInfo: function ({ action = "", url, method = "get", type, params, reusult }) {
        if (baseUrl.endsWith('/') && action.startsWith('/'))
            action = action.substring(1);
        else if (!baseUrl.endsWith('/') && !action.startsWith('/'))
            action = '/' + action;

        this.url = url || (baseUrl + action);
        this.method = method;
        this.params = params;
        this.reusult = reusult;
        this.type = type;
    },
    /** 一个标准化的请求结果ServiceResult结构
     * @class
     * @param {Boolen} success 是否是成功的，默认为false
     * @param {String} message 错误信息或成功提示
     * @param {*} datas 请求返回的数据内容
     */
    ServiceResult: function ({ success = false, message, datas }) {
        if (success == null)
            success = false;
        this.success = success;
        this.message = message || (success ? "操作成功" : "操作失败");
        this.datas = datas;
    }
}

export default contract;