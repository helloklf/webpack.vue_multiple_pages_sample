import contract from "./navigate.contract.js";

function load(file) {
    return require('../../pages/' + file);
}

export default {
    home: new contract.contente({ icon: "", title: "首页", content: load("Home.vue"), isDefault: true }),
    dzj: new contract.contente({ icon: "", title: "硕粉大赚家", content: load("DZJ.vue") }),
    recharge: new contract.contente({ icon: '', title: "充值服务", content: load("Recharge.vue") }),
    mphone: new contract.contente({
        icon: '', title: "手机助手", content: load("mphone/Enter.vue"),
        contents: {
            app2: new contract.contente({ icon: '', title: "应用管理", content: load("mphone/AppManage.vue") }),
            file: new contract.contente({ icon: '', title: "文件管理", content: load("mphone/FileManage.vue") }),
            /*contact: new contract.contente({
                icon: '', title: "通讯管理", content: load("mphone/Contact.vue"),
                contents: {
                    sms: new contract.contente({ title: "短信", content: load('mphone/contract.part/SMS.vue') }),
                    addressbook: new contract.contente({ title: "通讯录", content: load('mphone/contract.part/AddressBook.vue') })
                }
            }),
            */
            onekey: new contract.contente({ icon: '', title: "一键服务", content: load("mphone/OneKey.vue") })
        }
    }),

    recharge2: new contract.contente({ icon: '', title: "热卖产品", content: load("HotProd.vue") }),
    feedback: new contract.contente({ icon: '', title: "意见反馈", content: load("Feedback.vue") }),
    voip: new contract.contente({ icon: '', title: "免费电话", content: load("Voip.vue") }),
    download: new contract.contente({ title: "任务管理", content: load("Download.vue"), visible: false })
};