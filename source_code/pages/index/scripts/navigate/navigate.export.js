import contract from './navigate.contract.js';
import contents from './navigate.framents.js';
import { viewstate } from 'javascript_ext';


let navConfig = new contract.NavgateInfos(contents);


function nextLevel(currentLeve, path) {
    if (currentLeve.contents[path] != null) {
        currentLeve.contents.forEach(item => {
            item.isCurrent = false;
        });
        currentLeve.frament = currentLeve.contents[path];
        currentLeve.contents[path].isCurrent = true;
        return true;
    }
    else if (path.includes('.')) {
        let pathFirst = path.substring(0, path.indexOf('.'));
        if (currentLeve.contents[pathFirst] != null) {
            currentLeve.contents.forEach(item => {
                item.isCurrent = false;
            });
            currentLeve.frament = currentLeve.contents[pathFirst];
            currentLeve.contents[pathFirst].isCurrent = true;
            return nextLevel(currentLeve.contents[pathFirst], path.substring(path.indexOf('.') + 1, path.length));
        }
        else {
            return false;
        }
    }
}

navConfig.navigateTo = function (pageKey, pushState) {
    if (!nextLevel(navConfig, pageKey)) {
        throw "无法打开指定页面";
    }

    navConfig.isopenview = false;

    if (pushState != false)
        viewstate.pushState(pageKey, "此参数似乎没什么用", "?content=" + pageKey);
};


/** 创建导航树
 * 
 */
function createNavTree(contents, plevel) {
    let prefix = plevel == null ? "" : (plevel + '.');

    for (let pageKey in contents) {
        let pk = prefix + pageKey;
        contents[pageKey].key = pk;
        contents[pageKey].open = function (event) {
            if (event instanceof Event)
                event.preventDefault();
            let e = new Event("navigateTo");
            e.key = pk;
            window.dispatchEvent(e);
            return false;
        };
        if (contents[pageKey].contents != null) {
            createNavTree(contents[pageKey].contents, (prefix + pageKey));
        }
    }
}

createNavTree(contents);

viewstate.onPopState((state)=>{
    navConfig.navigateTo(state,false);
})

export default navConfig;