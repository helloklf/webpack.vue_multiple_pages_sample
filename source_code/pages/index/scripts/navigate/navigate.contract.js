let contract = {
    /**
     * @class
     * @param {String} icon 图标
     * @param {String} title 标题
     * @param {*} content 页面
     * @param {*} contents 子页面
     * @param {Boolean} isDefault 是否是默认页 
     * @param {Boolean} visible 是否显示在导航列表 
     */
    contente: function ({ icon, title, content, contents, isDefault, visible = true }) {
        if (contents instanceof Array)
            throw "contents应该是一个Object";

        this.icon = icon;
        this.title = (title == null ? "自定义页" : title);
        this.content = content;
        this.isCurrent = isDefault || false;
        this.contents = (contents || {});
        this.frament = (contents ? (contents[(contents.keys()[0])]) : null);
        this.visible = visible != false;
    },
    NavgateInfos: function (contents) {
        this.isopenview = false;
        this.frament = null;
        this.contents = contents;
        this.navigateTo = null;
    }
}

export default contract;