import navConfig from './navigate/navigate.export.js';


//打开页面时根据链接自动导航
if (window.location.href.includes('content=')) {
    let href = window.location.href;
    href = href.substring(href.indexOf('content=') + 8);

    if (href.includes("&"))
        href = href.substring(0, href.indexOf('&'));
    else if (href.includes("#"))
        href = href.substring(0, href.indexOf('#'));

    navConfig.navigateTo(href);
}
else {
    navConfig.frament = navConfig.contents["home"];
}

window.addEventListener("navigateTo", (e) => {
    if (e.key)
        navConfig.navigateTo(e.key);
    else {
        throw "未对Even实例设置key属性，无法找到页面！";
    }
});

export default navConfig;