import service from './service/service.export.js';

/**
 * 发起一个接口请求
 * @param {String} token api声明使用的名称，在service.localapi中定义
 * @param {*} datas 要提交的额数据内容
 * @param {Function|Null} onSuccess 请求成功时的回调
 * @param {Function|Null} onFailed 请求失败时的回调
 */
function request(token, datas, onSuccess, onFailed) {
    let api = service.localapi[token];
    if (!api) {
        throw `未找到声明为${token}的接口！`;
    }

    service.ajaxrequest(api, (r) => {
        if (r.success) {
            if (onSuccess && onSuccess instanceof Function)
                onSuccess(r);
            else if (onFailed instanceof Function)
                onFailed(r);
        }
    }, datas)
}

export default request;